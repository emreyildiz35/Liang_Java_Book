package chapter2;

public class DisplayCurrentTime {
    private int currentSecond;
    private int currentMinute;
    private int currentHour;
    private int currentTime;
    
    public String getCurrentTime(){
	currentTime = (int)(System.currentTimeMillis()/1000);
	currentSecond = currentTime % 60;
	currentMinute = (currentTime / 60) % 60;
	currentHour = (currentTime / 3600) % 24;
	return  currentHour + ":" + currentMinute + ":" + currentSecond;
		
    }
    

}
